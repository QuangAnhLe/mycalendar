/**
 * Connect to the database
 */

package com.example.quangale.mynotizen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class AppointmentsDataSource{

    public AppointmentsDataSource(Context context) {
        Log.d(LOG_TAG, "Unsere DataSource erzeugt jetzt den dbHelper.");
        dbHelper = new AppointmentsDbHelper(context);
    }

    //Open an database connection
    public void open() {
        Log.d(LOG_TAG, "Eine Referenz auf die Datenbank wird jetzt angefragt.");
        database = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "Datenbank-Referenz erhalten. Pfad zur Datenbank: " + database.getPath());
    }

    //Close the database connection
    public void close() {
        dbHelper.close();
        Log.d(LOG_TAG, "Datenbank mit Hilfe des DbHelpers geschlossen.");
    }


    //Save appointment into database
    public AppointmentsObject createAppointment(long _date, String _title, String _place, int _colorId) {
        ContentValues values = new ContentValues();
        values.put(AppointmentsDbHelper.COLUMN_DATE, _date);
        values.put(AppointmentsDbHelper.COLUMN_TITLE, _title);
        values.put(AppointmentsDbHelper.COLUMN_PLACE, _place);
        values.put(AppointmentsDbHelper.COLUMN_COLORID, _colorId);

        //Get the id of the row
        long insertId = database.insert(AppointmentsDbHelper.TABLE_APPOINTMENTS, null, values);


        Cursor cursor = database.query(AppointmentsDbHelper.TABLE_APPOINTMENTS,
                columns, AppointmentsDbHelper.COLUMN_ID + "=" + insertId,
                null, null, null, null);

        cursor.moveToFirst();
        AppointmentsObject appointmentsObject = cursorToAppointment(cursor);
        cursor.close();

        return appointmentsObject;
    }


    //returns all objects from db
    public List<AppointmentsObject> getAllAppointments() {
        List<AppointmentsObject> appointmentsObjectList = new ArrayList<>();

        Cursor cursor = database.query(AppointmentsDbHelper.TABLE_APPOINTMENTS,
                columns, null, null, null, null, null);

        cursor.moveToFirst();
        AppointmentsObject appointmentsObject;

        while(!cursor.isAfterLast()) {
            appointmentsObject = cursorToAppointment(cursor);
            appointmentsObjectList.add(appointmentsObject);
            Log.d(LOG_TAG, "ID: " + appointmentsObject.getId() + ", Inhalt: " + appointmentsObject.toString());
            cursor.moveToNext();
        }

        cursor.close();

        return appointmentsObjectList;
    }


//--------------------------------------------------------------

    private static final String LOG_TAG = AppointmentsDataSource.class.getSimpleName();

    private SQLiteDatabase database;
    private AppointmentsDbHelper dbHelper;

    private String[] columns = {
            AppointmentsDbHelper.COLUMN_ID,
            AppointmentsDbHelper.COLUMN_DATE,
            AppointmentsDbHelper.COLUMN_TITLE,
            AppointmentsDbHelper.COLUMN_PLACE,
            AppointmentsDbHelper.COLUMN_COLORID,
    };

    private AppointmentsObject cursorToAppointment(Cursor cursor) {
        //int idIndex     = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_ID);
        int idDate      = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_DATE);
        int idTitle     = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_TITLE);
        int idPlace     = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_PLACE);
        int idColorId   = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_COLORID);

        //long id         = cursor.getLong(idIndex);
        long date       = cursor.getLong(idDate);
        String title    = cursor.getString(idTitle);
        String place    = cursor.getString(idPlace);
        int colorId     = cursor.getInt(idColorId);

        AppointmentsObject appointmentsObject = new AppointmentsObject(date, title, place, colorId);

        return appointmentsObject;
    }
}