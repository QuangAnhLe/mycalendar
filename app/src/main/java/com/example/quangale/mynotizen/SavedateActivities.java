package com.example.quangale.mynotizen;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import static android.graphics.PixelFormat.TRANSPARENT;

public class SavedateActivities extends AppCompatActivity {

    /* Display date of appointment*/
    private TextView theDate;
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListerner;
    private TextView mDisplayTime_begin;
    private TimePickerDialog.OnTimeSetListener mTimeSetListerner_begin;
    private TextView mDisplayTime_end;
    private TimePickerDialog.OnTimeSetListener mTimeSetListerner_end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_savedate);
        theDate = (TextView) findViewById(R.id.editText4);

        Intent incomingIntent = getIntent();
        String date = incomingIntent.getStringExtra("date");
        theDate.setText(date);

        /*alternative choice date*/
        mDisplayDate=(TextView) findViewById(R.id.editText4);
        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(SavedateActivities.this,mDateSetListerner,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }
        });

        mDateSetListerner = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Log.d(TAG,"onDateSet: dd/mm/yyy"+ dayOfMonth + "." +(month+1)+"."+year);
                String date = dayOfMonth+ "." + (month + 1) + "." + year;
                mDisplayDate.setText(date);
            }
        };
        /*choice time beginning*/
        mDisplayTime_begin=(TextView) findViewById(R.id.editText5);
        //mDisplayTime_begin=(TextView) findViewById(R.id.editText6);
        mDisplayTime_begin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog(SavedateActivities.this,mTimeSetListerner_begin,hour,minute,true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }
        });

        mTimeSetListerner_begin = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time = hourOfDay+":"+minute;
                mDisplayTime_begin.setText(time);
            }
        };
        /*choice time ending*/
        //mDisplayTime_end=(TextView) findViewById(R.id.editText5);
        mDisplayTime_end=(TextView) findViewById(R.id.editText6);
        mDisplayTime_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog(SavedateActivities.this,mTimeSetListerner_end,hour,minute,true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }
        });

        mTimeSetListerner_end = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time = hourOfDay+":"+minute;
                mDisplayTime_end.setText(time);
            }
        };

        final EditText etTitle = (EditText) findViewById(R.id.etTitle);
        final EditText etPlace = (EditText) findViewById(R.id.etPlace);

        //create Spinner for repeat
        Spinner mySpinnerRepeat = (Spinner) findViewById(R.id.spinner1);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(SavedateActivities.this,
                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.repeat));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinnerRepeat.setAdapter(myAdapter);

        //create Spinner for reminder
        Spinner mySpinnerReminder = (Spinner) findViewById(R.id.spinner2);

        ArrayAdapter<String> myAdapter2 = new ArrayAdapter<String>(SavedateActivities.this,
                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.reminder));
        myAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinnerReminder.setAdapter(myAdapter2);



    }
}


