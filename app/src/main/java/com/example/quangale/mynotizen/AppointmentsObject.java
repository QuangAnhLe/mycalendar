/**
 * This class representate an java object filled with data from the database
 */

package com.example.quangale.mynotizen;

import java.util.Date;

public class AppointmentsObject {

    public AppointmentsObject(long _date, String _title, String _place, int _colorId){
        date    = _date;
        title   = _title;
        place   = _place;
        colorId = _colorId;
    }

    public long getId() { return id; }

    public String getPlace() { return place; }

    public long getDate() { return date; }

    public String getTitle() { return title; }

    public int getColorId() { return colorId; }

    public void setTitle(String _title) { this.title = _title; }

    public void setDate(long _date) { this.date = _date; }

    public void setId(long _id) { this.id = _id; }

    public void setPlace(String _place) { this.place = _place; }

    public void setColorId(int _colorId) { this.colorId = _colorId; }

    @Override
    public String toString() {
        return new Date(date) + ": " + title + ", Ort: " + place;
    }

    private long    id;
    private long    date;
    private String  title;
    private String  place;
    private int     colorId;
}
