/**
 * Defining the settings of our databse
 */

package com.example.quangale.mynotizen;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AppointmentsDbHelper extends SQLiteOpenHelper{

    //Filename of database
    public static final String DB_NAME =            "appoinments.db";
    public static final int DB_VERSION =            1;

    //Name of columns in table
    public static final String TABLE_APPOINTMENTS = "APPOINTMENTS";
    public static final String COLUMN_ID          = "_id";
    public static final String COLUMN_DATE        = "DATE";
    public static final String COLUMN_TITLE       = "TITLE";
    public static final String COLUMN_PLACE       = "PLACE";
    public static final String COLUMN_COLORID     = "COLORID";



    public static final String SQL_CREATE =
            "CREATE TABLE " + TABLE_APPOINTMENTS +
                    "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_DATE +     " SIGNED BIGINT NOT NULL, " +
                    COLUMN_TITLE +    " TEXT NOT NULL, " +
                    COLUMN_PLACE +    " TEXT NOT NULL, " +
                    COLUMN_COLORID +  " INTEGER NOT NULL);";


    //Create the database file (if not created)
    public AppointmentsDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.d(LOG_TAG, "DbHelper hat die Datenbank: " + getDatabaseName() + " erzeugt.");
    }


    // onCreate-method will only called, if the database not exist
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.d(LOG_TAG, "Die Tabelle wird mit SQL-Befehl: " + SQL_CREATE + " angelegt.");
            db.execSQL(SQL_CREATE);
        }
        catch (Exception ex) {
            Log.e(LOG_TAG, "Fehler beim Anlegen der Tabelle: " + ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private static final String LOG_TAG = AppointmentsDbHelper.class.getSimpleName();

}

