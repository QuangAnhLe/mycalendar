package com.example.quangale.mynotizen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;

import java.util.Date;
import java.util.List;


public class CalendarActivity extends AppCompatActivity {

    //Datenbankteil
    public static final String LOG_TAG = CalendarActivity.class.getSimpleName(); //Log tag for filtering
    private AppointmentsDataSource dataSource;
    //Datenbankteil

    CalendarView calendarView;
    TextView saveMyDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        //Datenbankteil
        //get the Datetime as long
        long date = new Date().getTime();

        //Intanciate a new dataSource-object
        dataSource = new AppointmentsDataSource(this);

        Log.d(LOG_TAG, "Datenquelle wird geöffnet");
        dataSource.open();

        //Write into database and save the object
        AppointmentsObject appointment = dataSource.createAppointment(date, "Testeintrag", "Altonaer Straße 1", 1);
        Log.d(LOG_TAG, "Es wurde der folgende Eintrag in die Datenbank geschrieben:");
        Log.d(LOG_TAG, "ID: " + appointment.getId() + ", Inhalt: " + appointment.toString());

        Log.d(LOG_TAG, "Folgende Einträge sind in der Datenbank vorhanden:");
        showAllListEntries();



        Log.d(LOG_TAG, "Datenquelle wird geschlossen");
        dataSource.close();
        //Datenbankteil


        final TextView bSaveMyDate = (TextView) findViewById(R.id.textView5);

        bSaveMyDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent saveDateIntent;
                saveDateIntent = new Intent(CalendarActivity.this,SavedateActivities.class);
                CalendarActivity.this.startActivity(saveDateIntent);
            }
        });

        calendarView =(CalendarView) findViewById(R.id.calendarView);
        //myDate = (TextView) findViewById(R.id.myDate);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

                //Log.d(TAG,"onDateSet: dd/mm/yyy"+ dayOfMonth + "." +(month+1)+"."+year);
                String date = dayOfMonth+ "." + (month + 1) + "." + year;
                //myDate.setText(date);

                Intent intent = new Intent(CalendarActivity.this,SavedateActivities.class);
                intent.putExtra("date",date);
                startActivity(intent);
            }
        });
    }


    //show a list view
    private void showAllListEntries () {
        List<AppointmentsObject> appointmentsObjectList = dataSource.getAllAppointments();

        //TODO Recycler View
//        ArrayAdapter<AppointmentsObject> appointmentsArrayAdapter = new ArrayAdapter<>(
//                this,
//                android.R.layout.simple_list_item_multiple_choice,
//                appointmentsObjectList);
//
//        ListView shoppingMemosListView = (ListView) findViewById(R.id.appointmentListView);
//        shoppingMemosListView.setAdapter(appointmentsArrayAdapter);
    }
}
